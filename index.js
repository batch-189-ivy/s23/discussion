//alert("hellow!")

/*
	OBJECTS
		An object is a data type that is used to represent real world objects. It is also a collection of related data and/or functionalities.

Creating objects using object literal
	Syntax:
		let objectName = {
			keyA: valueA,
			keyB: valueB
		}



*/


let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999,
}

console.log("Result from creating objects");
console.log(cellphone);
console.log(typeof cellphone);


/*
Creating objects using constructor function

		Creates a reusable function to create a several objects that have the same data structure. This is useful for creating multiple instances/copies of an object.

		Syntax:
			function ObjectName (KeyA, KeyB) {
				this.KeyA = KeyA,
				this.KeyB = KeyB
			}



*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate
}

let laptop = new Laptop("Lenovo", 2008);

console.log("Result of creating objects using object constructors");
console.log(laptop)

let myLaptop = new Laptop("MacBook Air", 2020)
console.log(myLaptop)

let oldLaptop = Laptop("Portal R2E CCMC", 1980)
console.log("Result from creating objects without the new keyword:")
console.log(oldLaptop) //undefined because we didn't add "new" before calling the function


// Object Methods
/*
	A method is a function which is a property of an object. They are also functions and one of the key differences is they is that the methods are functions related to our specific object.

*/

let person = {
	name: "John",
	talk: function(){
		console.log("Hello! My name is " + this.name);
	}
}
 console.log(person);
console.log("Result from object methods: ")
person.talk()

person.walk = function(){
	console.log(this.name + " walked 25 steps forward.");
}
person.walk()



// Real World Application of Objects
/*
	Scenario:
		1. We would like to create a game that would have several pokemon that interacts with each other.
		2. Every pokemon would have the same set of stats, properties and functions

*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon")
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	},
	faint: function(){
		console.log("Pokemon fainted.");
	}
}
console.log(myPokemon)
// creating an object constructor

function Pokemon(name,level){
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;


	//Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
		target.health = (target.health - this.attack)
		if(target.health <= 0) {
			this.faint(target.name)
		}
	},

	this.faint = function(pokemon){
			console.log(pokemon + " fainted")}
}

let pikachu = new Pokemon ("Pikachu", 16);
let squirtle = new Pokemon ("Squirtle", 8);
console.log(pikachu)
console.log(squirtle)

pikachu.tackle(squirtle)
pikachu.tackle(squirtle)





